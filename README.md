# Guayaquil City Table Tennis Club - Rankings Web Site

<!-- TOC -->
* [Guayaquil City Table Tennis Club - Rankings Web Site](#guayaquil-city-table-tennis-club---rankings-web-site)
  * [Overview](#overview)
<!-- TOC -->
<br/>

> This repository is one of a collection of three that support this solution:
>
> - The [GCTTC Rankings Tool] repository: The program used for the calculation.
> - The [GCTTC Rankings Data] repository: The initial and tournament reported data.
> - The [GCTTC Rankings Web] repository: The calculated data presentation web application.

## Overview

This is the repository of the **GCTTC Rankings Site**. This is where you can review the result of the data processed 
Tennis ranking held by [Guayaquil City Table Tennis Club]. 

> [!NOTE]  
> You can visit the site here: [https://gcttc.gitlab.io/gcttc-rankings-web/]

[GCTTC Rankings Tool]: https://gitlab.com/gcttc/gcttc-rankings

[GCTTC Rankings Data]: https://gitlab.com/gcttc/gcttc-rankings-data

[GCTTC Rankings Web]: https://gitlab.com/gcttc/gcttc-rankings-web

[Guayaquil City Table Tennis Club]: https://www.instagram.com/guayaquilcitytabletennisclub

[https://gcttc.gitlab.io/gcttc-rankings-web/]: https://gcttc.gitlab.io/gcttc-rankings-web/